console.log('loaded js file');

var submitButton = document.getElementById('bsr-submit-button');
submitButton.onmouseup = getFormInfo;

var clearButton = document.getElementById('bsr-clear-button');
clearButton.onclick = resetUI();

function resetUI(){
    // clear all the innerHTML strings for info display
    var response_status = document.getElementById('response-status');
    var response_price = document.getElementById('response-price');
    var response_name = document.getElementById('response-name');
    var response_enrollment = document.getElementById('response-enrollment');
    var response_grad_rate = document.getElementById('response-grad-rate');
    var response_url = document.getElementById('response-link');
    var response_img = document.getElementById('response-image');
    response_status.innerHTML = ' ';
    response_price.innerHTML = ' ';
    response_name.innerHTML = ' ';
    response_enrollment.innerHTML = ' ';
    response_grad_rate.innerHTML = ' ';
    response_url.innerHTML = ' ';
    response_img.innerHTML = ' ';
}

function getFormInfo() {
    // type of request
    var all = 0;
    var request_type = "GET";
    var id = null;
    if(document.getElementById('get-button').checked) {
        request_type = "GET";
        id = document.getElementById('id-text').value;
    } else if(document.getElementById('get-all-button').checked) {
        all = 1;
        request_type = "GET";
    }/* else if(document.getElementById('post-button').checked) {
        request_type = "POST";
    } else if(document.getElementById('delete-button').checked) {
        request_type = "DELETE";
    }*/

    // id if applicable
    /*
    var id = null;
    if(document.getElementById('use-id').checked && !all) {
        id = document.getElementById('id-text').value;
    }
    */

    // body if applicable
    var body = null;
    /*
    if(document.getElementById('use-body').checked) {
        body = document.getElementById('body').value;
    }
    */

    console.log(request_type);
    console.log(id);
    console.log(body);
    makeRestCall(request_type, id, body, all);
}

// add error checking

function makeRestCall(request_type, id, body, all) {
    console.log('entered first rest call');
    // set up url
    if(id){
        var url = "http://localhost:51045/schools/" + id;
    }else{
        var url = "http://localhost:51045/schools/";
    }
    // set up request
    var xhr = new XMLHttpRequest();
    xhr.open(request_type, url, true);

    // to be executed on click
    xhr.onload = function(e){
        console.log('first network response received');
        updateWithRestResponse(xhr.responseText, all);
    }

    xhr.onerror = function (e) {
        console.error(xhr.statusText);
    }
    // send request body
    xhr.send(body);
}

function updateWithRestResponse(response_text, all) {
    console.log('updating page with response');
    // parse json and set display values accordingly

    var response_json = JSON.parse(response_text);
    var response_status = document.getElementById('response-status');
    if (response_json['result'] != 'success') {
        response_status.innerHTML = 'We\'re sorry, we were not able to complete your request'
    } else {
        response_status.innerHTML = 'Enjoy this info and pic of a random dog';
    }
    var response_all_ids = document.getElementById('response-all-ids');
    response_all_ids.innerHTML = null;
    if (all) {
        var schools = {}
        for(let id in response_json){
            if(id=='result'){
                continue;
            }
            schools[id] = response_json[id]['Name'];
        }
        console.log(JSON.stringify(schools))

        response_all_ids.innerHTML = JSON.stringify(schools);
    } else {
        for (let key in response_json['school']) {
            switch (key) {
                case "Name":
                    var response_name = document.getElementById('response-name');
                    response_name.innerHTML = 'College Name: ' + response_json['school'][key];
                    break;
                case "Enrollment":
                    var response_enrollment = document.getElementById('response-enrollment');
                    response_enrollment.innerHTML = 'Enrollment: ' + response_json['school'][key];
                    break;
                case "Graduation Rate":
                    var response_grad_rate = document.getElementById('response-grad-rate');
                    response_grad_rate.innerHTML = 'Graduation Rate: ' + response_json['school'][key];
                    break;
                case "Price":
                    var response_price = document.getElementById('response-price');
                    response_price.innerHTML = 'Cost of Attendance: ' + response_json['school'][key];
                    break;
                case "Url":
                    var response_url = document.getElementById('response-link');
                    response_url.href = response_json['school'][key];
                    response_url.innerHTML = 'Learn More';
                    break;
                case "Image":
                    var response_img = document.getElementById('response-image');
                    response_img.src = response_json['school'][key];
                default:
                    break;

            }
        }
    }
    // second api call
    makeSecondCall();
}

function makeSecondCall() {
    console.log('entered second rest call');
    // set up url
    var url = 'https://dog.ceo/api/breeds/image/random'
    // set up request
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    // get response on click
    xhr.onload = function(e){
        console.log('second network response received');
        updateWithSecondResponse(xhr.responseText);
    }

    xhr.onerror = function(e){
        console.error(xhr.statusText);
    }
    // send no body
    xhr.send(null);
}

function updateWithSecondResponse(response_text){
    response_json = JSON.parse(response_text);
    // update image with response from dog api
    var response_status = document.getElementById('response-status');
    if(response_json['status'] != 'success'){
        response_status.innerHTML = 'Sorry we could not get a picture of a dog. Here is the requested info';
    }

    new_image = document.createElement("img");
    new_image.setAttribute("src", response_json['message']);

    var response_div = document.getElementById('response-div');
    response_div.appendChild(new_image);
    /*
    var response_img = document.getElementById('response-image');
    response_img.src = response_json['message'];
    */
}
